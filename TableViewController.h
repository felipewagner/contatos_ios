//
//  TableViewController.h
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/12/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@property (strong,nonatomic) NSDictionary* agenda;

@end
