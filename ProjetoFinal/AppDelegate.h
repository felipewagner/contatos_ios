//
//  AppDelegate.h
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/11/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
