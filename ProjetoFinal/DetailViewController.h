//
//  DetailViewController.h
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/15/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pessoa.h"

@interface DetailViewController : UIViewController

@property (nonatomic,strong) Pessoa* pessoa;
@property (nonatomic,strong) NSString* teste;

@property (weak, nonatomic) IBOutlet UILabel *lblTelefones;
@property (weak, nonatomic) IBOutlet UILabel *lblEmails;
@end
