//
//  FWHttpUtils.h
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/12/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^ResponseHTTPBlock)(NSDictionary*);

@interface FWHttpUtils : NSObject

// FW Send a POST with a urlencoded postdata
// url: @"www.google.com"
// data: "usuario=hahaha&senha=hohohoho"
//
// TODO: convert data to a Dictionary, then serialize the dictionary
/* ex:
    {
        "usuario":"felipew",
        "senha":"qwerty"
    }
 */
-(void) sendPOSTHttpRequestWithUrl:(NSString*)stringUrl andData:(NSString*)data successCallback:(ResponseHTTPBlock)successCallback errorCallback:(ResponseHTTPBlock)erroCallback;

@end
