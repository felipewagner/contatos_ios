//
//  FWHttpUtils.m
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/12/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import "FWHttpUtils.h"



@implementation FWHttpUtils

-(void) sendPOSTHttpRequestWithUrl:(NSString*)stringUrl andData:(NSString*)data successCallback:(ResponseHTTPBlock)successCallback errorCallback:(ResponseHTTPBlock)errorCallback {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:stringUrl]];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               //                               NSString *requestData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                               NSError* jsonError;
                               id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
                               if(jsonError){
                                   NSLog(@"Deu erro %@",[jsonError description]);
                               }
                               
                               NSDictionary* mainDict = [[NSDictionary alloc] initWithDictionary:jsonObject];
                               NSInteger result = [[mainDict objectForKey:@"result"] integerValue];
                               
                               if( result == 0 ){
                                   errorCallback(mainDict);
                               } else {
                                   successCallback(mainDict);
                               }
                           }];
}
@end
