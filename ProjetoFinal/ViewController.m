//
//  ViewController.m
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/11/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import "ViewController.h"
#import "FWHttpUtils.h"
#import "TableViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissKeyboard:(id)sender {
    // Dismiss keyboard :)
    [self.login resignFirstResponder];
    [self.password resignFirstResponder];
}

- (IBAction)loginService:(id)sender {
    [self dismissKeyboard:nil];
    
    if( ![self validaCamposLogin] ) {
        NSLog(@"Login Incorreto");
        return;
    }
    
    [self toogleCampos:NO];
    
    [self efetuaLoginNoWebServer];
}

- (IBAction)createLoginService:(id)sender {
    [self dismissKeyboard:nil];
    
    if( ![self validaCamposLogin] ) {
        NSLog(@"Login Incorreto");
        return;
    }
    
    [self toogleCampos:NO];
    
    [self criaLoginNoWebServer];
}

- (BOOL) validaCamposLogin {
    // Check login
    if( [[self.login text] length] < 1 ) {
        [self mostraMensagemComTitulo:@"Atenção" andMessage:@"Favor preencher o login"];
        return FALSE;
    }

    if( [[self.password text] length] < 1 ){
        [self mostraMensagemComTitulo:@"Atenção" andMessage:@"Favor preencher a senha"];
        return FALSE;
    }
    
    return TRUE;
}

-(void) efetuaLoginNoWebServer {
    NSString* login = [self.login text];
    NSString* pass  = [self.password text];
    NSString* postData = [[NSString alloc] initWithFormat:@"usuario=%@&senha=%@",login,pass];
    
    FWHttpUtils* http = [[FWHttpUtils alloc]init];
    
    [http sendPOSTHttpRequestWithUrl:@"http://iai.art.br/aula/login.php" andData:postData successCallback:^(NSDictionary * dict) {
        self.agenda = [[NSDictionary alloc] initWithDictionary:dict];
        [self performSegueWithIdentifier:@"loginSegue" sender:nil];
        [self toogleCampos:YES];
    } errorCallback:^(NSDictionary * dict) {
        [self mostraMensagemComTitulo:@"Falha" andMessage:[dict valueForKey:@"msg"]];
        [self toogleCampos:YES];
    }];
}

-(void) criaLoginNoWebServer {
    NSString* login = [self.login text];
    NSString* pass  = [self.password text];
    NSString* postData = [[NSString alloc] initWithFormat:@"usuario=%@&senha=%@",login,pass];
    
    FWHttpUtils* http = [[FWHttpUtils alloc]init];
    
    [http sendPOSTHttpRequestWithUrl:@"http://iai.art.br/aula/cadastrar.php" andData:postData successCallback:^(NSDictionary * dict) {
        
        NSNumber* result = [dict valueForKey:@"result"];
        
        if([result integerValue] != 1){
            NSLog(@"FUDEU");
        }
        [self mostraMensagemComTitulo:@"Sucesso!" andMessage:[dict valueForKey:@"msg"]];
        [self toogleCampos:YES];
    } errorCallback:^(NSDictionary * dict) {
        [self mostraMensagemComTitulo:@"Falha" andMessage:[dict valueForKey:@"msg"]];
        [self toogleCampos:YES];
    }];
}

-(void) toogleCampos: (BOOL) opcao {
    [self.btnLogin setEnabled:opcao];
    [self.btnCriar setEnabled:opcao];
}


#pragma mark Utils
-(void) mostraMensagemComTitulo: (NSString*)titulo andMessage:(NSString*)msg {
    [[[UIAlertView alloc] initWithTitle:titulo message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark SegueHandler
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if( [segue.identifier isEqualToString:@"loginSegue"] ) {
        UINavigationController* nv = [segue destinationViewController];             // recupera o navigation controller
        TableViewController* tv = (TableViewController *) [nv viewControllers][0];  // pega o primeiro view apresentado
        [tv setAgenda:self.agenda]; // seta o atributo dele
        NSLog(@"%@",[tv agenda]);
        NSLog(@"%@",self.agenda);
    }
}
@end
