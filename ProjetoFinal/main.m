//
//  main.m
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/11/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
