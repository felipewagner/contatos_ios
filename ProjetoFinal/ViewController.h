//
//  ViewController.h
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/11/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)loginService:(id)sender;
- (IBAction)createLoginService:(id)sender;

@property (strong,nonatomic) NSDictionary* agenda;
@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnCriar;

@end
