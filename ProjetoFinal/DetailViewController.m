//
//  DetailViewController.m
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/15/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import "DetailViewController.h"
#import "Pessoa.h"

@interface DetailViewController ()
@end

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self carregaDados];
    
    NSString* usuario = [[NSString alloc] initWithFormat:@"%@",[_pessoa usuario]];
    [self setTitle:usuario];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) carregaDados {
    if([[_pessoa telefones] count] > 0) {
        // Carrega telefones
        NSString* textTelefones = @"";
        for(int i=0; i < [[_pessoa telefones] count] ; i++){
            NSString* teste = [[NSString alloc]initWithFormat:@"%@\n",[_pessoa telefones][i][@"telefone"]];
            textTelefones = [textTelefones stringByAppendingFormat:@"%@",teste];
        }
        [_lblTelefones setText:textTelefones];
    }
    
    if([[_pessoa emails]count] > 0){
        // carrega emails
        NSString* textEmails = @"";
        for(int i=0; i < [[_pessoa emails] count] ; i++){
            NSString* teste = [[NSString alloc]initWithFormat:@"%@\n",[_pessoa emails][i][@"email"]];
            textEmails = [textEmails stringByAppendingFormat:@"%@",teste];
        }
        [_lblEmails setText:textEmails];
    }
}

@end
