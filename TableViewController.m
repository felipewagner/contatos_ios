//
//  TableViewController.m
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/12/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import "TableViewController.h"
#import "FWHttpUtils.h"
#import "Pessoa.h"
#import "DetailViewController.h"

@interface TableViewController () {
    NSMutableArray* arrayContatos;
}

@end

@implementation TableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrayContatos = [[self.agenda objectForKey:@"dados"] mutableCopy];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"%d",[arrayContatos count]);
    return [arrayContatos count];
}

-(void) logout {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contatoCell"];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"contatoCell"];
    }
    
    NSDictionary* contato = [arrayContatos objectAtIndex:indexPath.row];
    
    NSString* nome = [contato valueForKey:@"usuario"];
    
    cell.textLabel.text = nome;

    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"detailSegue" sender:nil];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        FWHttpUtils* http = [[FWHttpUtils alloc] init];
        
        NSString* nome = [arrayContatos[indexPath.row] valueForKey:@"usuario"];
        NSString* postData = [[NSString alloc] initWithFormat:@"usuario=%@",nome];
        
        [http sendPOSTHttpRequestWithUrl:@"http://iai.art.br/aula/deleteUser.php" andData:postData successCallback:^(NSDictionary * dict) {
            [arrayContatos removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        } errorCallback:^(NSDictionary *dict ) {
            NSLog(@"deu merda..");
        }];
        

    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if( [[segue identifier] isEqualToString:@"detailSegue"] ) {
        NSIndexPath* indexpath = [self.tableView indexPathForSelectedRow];
        NSDictionary* dictPessoa = [arrayContatos objectAtIndex:indexpath.row];
        Pessoa* pessoa = [[Pessoa alloc] init];
        
        NSLog(@"%@",dictPessoa);
        
        [pessoa setUsuario:[dictPessoa objectForKey:@"usuario"]];
        [pessoa setTelefones: [dictPessoa objectForKey:@"telefones"]];
        [pessoa setEmails: [dictPessoa objectForKey:@"emails"]];
        
        [[segue destinationViewController] setPessoa:pessoa];
        [[segue destinationViewController] setTeste:@"testando123"];
    }
}

@end
