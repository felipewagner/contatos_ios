//
//  Pessoa.h
//  ProjetoFinal
//
//  Created by Felipe Wagner on 8/12/14.
//  Copyright (c) 2014 narf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (nonatomic,weak) NSNumber* id;
@property (nonatomic,weak) NSString* usuario;
@property (nonatomic,weak) NSArray* telefones;
@property (nonatomic,weak) NSArray* emails;

@end
